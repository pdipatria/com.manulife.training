(function() {
	'use strict';
             
	angular
		.module('com.finantix.problemandsolution', [
		                                             'com.finantix.problemandsolution.problem'
		                                             ]);
})();
(function() {
	'use strict';

	angular
		.module('com.finantix.problemandsolution')
	    .factory('problemAndSolutionResourceService', ['resourcePrefixIndex', function(resourcePrefixIndex) {
	    	return resourcePrefixIndex.createModuleResourceService('com.finantix.problemandsolution');
	    }]);
})(); 
(function() {	
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem', [
                                'com.finantix.shared',
                                'com.finantix.problemandsolution.problem.header',
                                'com.finantix.problemandsolution.problem.content'
                                ]);
})();
(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem')
		.factory('abstractProblemSharedStateService', abstractProblemSharedStateService);
	
	abstractProblemSharedStateService.$inject = ['observerFactory'];
	
	function abstractProblemSharedStateService(observerFactory) {
		var observer = observerFactory.createService();
		
		return {
			headerBackButtonClicked: headerBackButtonClicked,
			subscribeHeaderBackButtonClicked: subscribeHeaderBackButtonClicked,
			unsubscribeHeaderBackButtonClicked: unsubscribeHeaderBackButtonClicked,
			_onSubscribeHeaderBackButtonClicked: _onSubscribeHeaderBackButtonClicked,
			_onUnsubscribeHeaderBackButtonClicked: _onUnsubscribeHeaderBackButtonClicked
		};
		
		function headerBackButtonClicked() {
			notifyeHeaderBackButtonClicked(false, true);
		}
		
		function subscribeHeaderBackButtonClicked(callback) {
			observer.subscribe("problemHeaderBackButtonClicked", callback);
			
			this._onSubscribeHeaderBackButtonClicked();
		}
		
		function unsubscribeHeaderBackButtonClicked(callback) {
			observer.unsubscribe("problemHeaderBackButtonClicked", callback);
			
			this._onUnsubscribeHeaderBackButtonClicked();
		}

		
		// Protected methods
        function notifyeHeaderBackButtonClicked(oldValue, newValue) {
            observer.notify('problemHeaderBackButtonClicked', {
                oldValue: oldValue,
                newValue: newValue
            });
        };
        
		function _onSubscribeHeaderBackButtonClicked() {
			
		}
		
		function _onUnsubscribeHeaderBackButtonClicked() {
			
		}
	};
})();
(function() {	
	'use strict';
	
	angular.module('com.finantix.problemandsolution.problem')
	.factory('abstractProblemListenerService', abstractProblemListenerService);
	
	abstractProblemListenerService.$inject = [];
	
	function abstractProblemListenerService() {		
		return {
			gotToSolution: gotToSolution,
			closeProblemPage: closeProblemPage
		};
		
		function gotToSolution(problem) {
			throwError('gotToSolution');
		}
		
		function closeProblemPage() {
			throwError('closeProblemPage');
		}
		
		function throwError(methodName) {
			var msg = 'abstract implementation of method "' +methodName+ '"invoked -> error';
    		throw msg;
		}
	}
})();
(function() {
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem.header', []);
})();
(function() {

	'use strict';

	angular.module('com.finantix.problemandsolution.problem.header')
			.controller('ProblemHeaderController', ProblemHeaderController);
	
	ProblemHeaderController.$inject = ['problemSharedStateService'];

	function ProblemHeaderController(problemSharedStateService) {
		var vm = this;
		
		vm.onBack = onBack;
		
		activate();
		
		function activate() {}
		
		function onBack() {
			problemSharedStateService.headerBackButtonClicked();
		}
	}

})();
(function() {	
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem.content', []);
})();
(function(){
	
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem.content')
		.controller('ProblemContentController', ProblemContentController);
	
	ProblemContentController.$inject = ['problemContentService', 'problemListenerService', 'problemSharedStateService', '$window', '$translate'];
	
	function ProblemContentController(problemContentService, problemListenerService, problemSharedStateService, $window, $translate) {
		var vm = this;
		
		vm.problems = [];
		vm.newProblem = {};
		vm.onProblemClick = onProblemClick;
		vm.addNewProblem = addNewProblem;
		
		activate();
		
		function activate() {
			problemSharedStateService.subscribeHeaderBackButtonClicked(manageUserExit);
			
			loadProblems();
		}
		
		function manageUserExit() {
			if(vm.newProblem.question) {
				$translate("apply.problem.newProblemFieldNotEmpty")
					.then(function(text) {
						$window.alert(text);
					});
			} else {
				problemListenerService.closeProblemPage();
			}
		}
		
		function loadProblems() {
			problemContentService
				.getProblems()
				.then(function(problems) {
					vm.problems = problems.values;
				});
		}
		
		function onProblemClick(problem) {
			problemListenerService.gotToSolution(problem);
		}
		
		function addNewProblem() {
			if(vm.newProblem.question) {
				problemContentService
					.saveNewProblem({question: vm.newProblem.question})
					.then(loadProblems);
			}
		}
	}
	
})();
(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem.content')
		.factory('abstractProblemContentService', abstractProblemContentService);
	
	abstractProblemContentService.$inject = [];
	
	function abstractProblemContentService() {
		return {
			getProblems: getProblems,
			saveNewProblem: saveNewProblem
		};
		
		function getProblems(query) {
			throwError('getProblems');
		}
		
		function saveNewProblem(query) {
			throwError('saveNewProblem');
		}
		
		function throwError(methodName) {
			var msg = 'abstract implementation of method "' +methodName+ '"invoked -> error';
    		throw msg;
		}
	};
})();

(function() {	
	'use strict';
	
	angular.module('com.finantix.problemandsolution.problem')
	.factory('problemListenerService', problemListenerService);
	
	problemListenerService.$inject = ['abstractProblemListenerService', '$state'];
	
	function problemListenerService(abstractProblemListenerService, $state) {		
		return angular.extend({}, abstractProblemListenerService, {
			gotToSolution: gotToSolution,
			closeProblemPage: closeProblemPage
		});
		
		function gotToSolution(problem) {
			$state.go('index.problem.solution', {problemId: problem.id});
		}
		
		function closeProblemPage() {
			$state.go('index.worklist');
		}
	};
})();
(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem')
		.factory('problemSharedStateService', problemSharedStateService);
	
	problemSharedStateService.$inject = ['abstractProblemSharedStateService'];
	
	function problemSharedStateService(abstractProblemSharedStateService) {
		return angular.extend({}, abstractProblemSharedStateService, {});
	}
})();
(function() {	
	'use strict';
	
	angular.module('com.finantix.problemandsolution.problem.content')
	.factory('problemContentService', problemContentService);
	
	problemContentService.$inject = ['abstractProblemContentService', '$q'];
	
	function problemContentService(abstractProblemContentService, $q) {	
		var problems = {
				values: [
				         { id: '1', question: 'Question 1?' },
				         { id: '2', question: 'Question 2?' },
				         { id: '3', question: 'Question 3?' },
				         { id: '4', question: 'Question 4?' },
				         { id: '5', question: 'Question 5?' },
				         { id: '6', question: 'Question 6?' }
		         ]
			};
		var problemIndex = 6;
		
		return angular.extend({}, abstractProblemContentService, {
			getProblems: getProblems,
			saveNewProblem: saveNewProblem
		});
		
		function getProblems(query) {
			return $q.when(problems);
		}
		
		function saveNewProblem(newProblem) {
			problemIndex += 1;
			problems.values.push({ 
				id: problemIndex + '', 
				question: newProblem.question
			});
			return $q.when({ok: true});
		}
	};
})();

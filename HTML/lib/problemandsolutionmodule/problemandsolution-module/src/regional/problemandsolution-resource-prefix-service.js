(function() {
	'use strict';

	angular
		.module('com.finantix.problemandsolution')
	    .factory('problemAndSolutionResourceService', ['resourcePrefixIndex', function(resourcePrefixIndex) {
	    	return resourcePrefixIndex.createModuleResourceService('com.finantix.problemandsolution');
	    }]);
})(); 
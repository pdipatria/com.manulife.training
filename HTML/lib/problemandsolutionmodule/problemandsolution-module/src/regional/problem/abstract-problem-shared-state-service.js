(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem')
		.factory('abstractProblemSharedStateService', abstractProblemSharedStateService);
	
	abstractProblemSharedStateService.$inject = ['observerFactory'];
	
	function abstractProblemSharedStateService(observerFactory) {
		var observer = observerFactory.createService();
		
		return {
			headerBackButtonClicked: headerBackButtonClicked,
			subscribeHeaderBackButtonClicked: subscribeHeaderBackButtonClicked,
			unsubscribeHeaderBackButtonClicked: unsubscribeHeaderBackButtonClicked,
			_onSubscribeHeaderBackButtonClicked: _onSubscribeHeaderBackButtonClicked,
			_onUnsubscribeHeaderBackButtonClicked: _onUnsubscribeHeaderBackButtonClicked
		};
		
		function headerBackButtonClicked() {
			notifyeHeaderBackButtonClicked(false, true);
		}
		
		function subscribeHeaderBackButtonClicked(callback) {
			observer.subscribe("problemHeaderBackButtonClicked", callback);
			
			this._onSubscribeHeaderBackButtonClicked();
		}
		
		function unsubscribeHeaderBackButtonClicked(callback) {
			observer.unsubscribe("problemHeaderBackButtonClicked", callback);
			
			this._onUnsubscribeHeaderBackButtonClicked();
		}

		
		// Protected methods
        function notifyeHeaderBackButtonClicked(oldValue, newValue) {
            observer.notify('problemHeaderBackButtonClicked', {
                oldValue: oldValue,
                newValue: newValue
            });
        };
        
		function _onSubscribeHeaderBackButtonClicked() {
			
		}
		
		function _onUnsubscribeHeaderBackButtonClicked() {
			
		}
	};
})();
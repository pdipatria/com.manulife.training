(function() {	
	'use strict';
	
	angular.module('com.finantix.problemandsolution.problem')
	.factory('abstractProblemListenerService', abstractProblemListenerService);
	
	abstractProblemListenerService.$inject = [];
	
	function abstractProblemListenerService() {		
		return {
			gotToSolution: gotToSolution,
			closeProblemPage: closeProblemPage
		};
		
		function gotToSolution(problem) {
			throwError('gotToSolution');
		}
		
		function closeProblemPage() {
			throwError('closeProblemPage');
		}
		
		function throwError(methodName) {
			var msg = 'abstract implementation of method "' +methodName+ '"invoked -> error';
    		throw msg;
		}
	}
})();

(function() {
	angular.module('com.finantix.problemandsolution.problem')
	.config(['abstractLocalizationServiceProvider', function(abstractLocalizationServiceProvider) {
		abstractLocalizationServiceProvider.addDictionary({
			"en": {
				"values": {
					"apply": {
						"problem": {
							"newProblemFieldNotEmpty": 'Please clear input field'
						}
					}
				}
			}
		});
	}]);
})();
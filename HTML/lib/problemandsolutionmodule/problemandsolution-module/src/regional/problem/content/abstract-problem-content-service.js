(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem.content')
		.factory('abstractProblemContentService', abstractProblemContentService);
	
	abstractProblemContentService.$inject = [];
	
	function abstractProblemContentService() {
		return {
			getProblems: getProblems,
			saveNewProblem: saveNewProblem
		};
		
		function getProblems(query) {
			throwError('getProblems');
		}
		
		function saveNewProblem(query) {
			throwError('saveNewProblem');
		}
		
		function throwError(methodName) {
			var msg = 'abstract implementation of method "' +methodName+ '"invoked -> error';
    		throw msg;
		}
	};
})();

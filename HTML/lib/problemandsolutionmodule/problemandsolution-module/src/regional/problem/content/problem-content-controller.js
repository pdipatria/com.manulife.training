(function(){
	
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem.content')
		.controller('ProblemContentController', ProblemContentController);
	
	ProblemContentController.$inject = ['problemContentService', 'problemListenerService', 'problemSharedStateService', '$window', '$translate'];
	
	function ProblemContentController(problemContentService, problemListenerService, problemSharedStateService, $window, $translate) {
		var vm = this;
		
		vm.problems = [];
		vm.newProblem = {};
		vm.onProblemClick = onProblemClick;
		vm.addNewProblem = addNewProblem;
		
		activate();
		
		function activate() {
			problemSharedStateService.subscribeHeaderBackButtonClicked(manageUserExit);
			
			loadProblems();
		}
		
		function manageUserExit() {
			if(vm.newProblem.question) {
				$translate("apply.problem.newProblemFieldNotEmpty")
					.then(function(text) {
						$window.alert(text);
					});
			} else {
				problemListenerService.closeProblemPage();
			}
		}
		
		function loadProblems() {
			problemContentService
				.getProblems()
				.then(function(problems) {
					vm.problems = problems.values;
				});
		}
		
		function onProblemClick(problem) {
			problemListenerService.gotToSolution(problem);
		}
		
		function addNewProblem() {
			if(vm.newProblem.question) {
				problemContentService
					.saveNewProblem({question: vm.newProblem.question})
					.then(loadProblems);
			}
		}
	}
	
})();

(function() {	
	'use strict';
	
	angular.module('com.finantix.problemandsolution.problem')
	.factory('problemListenerService', problemListenerService);
	
	problemListenerService.$inject = ['abstractProblemListenerService', '$state'];
	
	function problemListenerService(abstractProblemListenerService, $state) {		
		return angular.extend({}, abstractProblemListenerService, {
			gotToSolution: gotToSolution,
			closeProblemPage: closeProblemPage
		});
		
		function gotToSolution(problem) {
			$state.go('index.problem.solution', {problemId: problem.id});
		}
		
		function closeProblemPage() {
			$state.go('index.worklist');
		}
	};
})();

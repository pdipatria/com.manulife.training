(function() {	
	'use strict';

	angular
		.module('com.finantix.problemandsolution.problem', [
                                'com.finantix.shared',
                                'com.finantix.problemandsolution.problem.header',
                                'com.finantix.problemandsolution.problem.content'
                                ]);
})();
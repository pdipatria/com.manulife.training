(function() {

	'use strict';

	angular.module('com.finantix.problemandsolution.problem.header')
			.controller('ProblemHeaderController', ProblemHeaderController);
	
	ProblemHeaderController.$inject = ['problemSharedStateService'];

	function ProblemHeaderController(problemSharedStateService) {
		var vm = this;
		
		vm.onBack = onBack;
		
		activate();
		
		function activate() {}
		
		function onBack() {
			problemSharedStateService.headerBackButtonClicked();
		}
	}

})();

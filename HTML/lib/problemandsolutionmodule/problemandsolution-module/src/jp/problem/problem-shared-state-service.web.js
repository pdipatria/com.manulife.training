(function() {	
	'use strict';
	
	angular
		.module('com.finantix.problemandsolution.problem')
		.factory('problemSharedStateService', problemSharedStateService);
	
	problemSharedStateService.$inject = ['abstractProblemSharedStateService'];
	
	function problemSharedStateService(abstractProblemSharedStateService) {
		return angular.extend({}, abstractProblemSharedStateService, {});
	}
})();
# WS call - REST entry point
# admin - create solution - no problem
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json -u fxposImport_HK:12345678 -d '{"type":"solution","code":"SOL001","description":"Solution 001","id":-1}'
# check created solution
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions/-1.json -u FA000001:12345678 -s | jq .

curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO001","description":"Problem 001","id":-1}'
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO002","description":"Problem 002","id":-2}'
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO003","description":"Problem 003","id":-3}'

curl -XGET http://127.0.0.1:8080/rest/problemandsolution/solutions/getSolutionDetail/SOL001.json -u FA000001:12345678 -s | jq .

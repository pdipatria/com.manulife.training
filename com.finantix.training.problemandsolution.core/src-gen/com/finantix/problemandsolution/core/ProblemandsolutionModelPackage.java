package com.finantix.problemandsolution.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.texo.model.ModelFactory;
import org.eclipse.emf.texo.model.ModelPackage;
import org.eclipse.emf.texo.model.ModelResolver;
import com.finantix.problemandsolution.core.dao.ProblemDao;
import com.finantix.problemandsolution.core.dao.SolutionDao;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.binding.IContainerList;
import com.thedigitalstack.model.internal.metadata.DaoRegistry;
import com.thedigitalstack.model.metadata.IDaoRegistry;
import com.thedigitalstack.model.metadata.TenantModelPackage;

/**
 * The <b>Package</b> for the model '<em><b>problemandsolution</b></em>'. It contains initialization code and access to the Factory to instantiate types of this
 * package.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
public class ProblemandsolutionModelPackage extends TenantModelPackage {

    /**
     * The package namespace URI.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    public static final String NS_URI = "com.finantix.problemandsolution.core.model";

    /**
     * The package name.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final String NAME = "problemandsolution";

    /**
     * The classifier id for the '{@link Solution <em>Solution</em>}' class.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_CLASSIFIER_ID = 0;

    /**
     * The feature id for the '{@link Solution <em>id</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_ID_FEATURE_ID = 0;

    /**
     * The feature id for the '{@link Solution <em>code</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_CODE_FEATURE_ID = 1;

    /**
     * The feature id for the '{@link Solution <em>description</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_DESCRIPTION_FEATURE_ID = 2;

    /**
     * The feature id for the '{@link Solution <em>deliveryDate</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_DELIVERYDATE_FEATURE_ID = 3;

    /**
     * The feature id for the '{@link Solution <em>releasedIn</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_RELEASEDIN_FEATURE_ID = 4;

    /**
     * The feature id for the '{@link Solution <em>problemRel</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int SOLUTION_PROBLEMREL_FEATURE_ID = 5;

    /**
     * The classifier id for the '{@link Problem <em>Problem</em>}' class.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int PROBLEM_CLASSIFIER_ID = 1;

    /**
     * The feature id for the '{@link Problem <em>id</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int PROBLEM_ID_FEATURE_ID = 0;

    /**
     * The feature id for the '{@link Problem <em>code</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int PROBLEM_CODE_FEATURE_ID = 1;

    /**
     * The feature id for the '{@link Problem <em>description</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int PROBLEM_DESCRIPTION_FEATURE_ID = 2;

    /**
     * The feature id for the '{@link Problem <em>lastUpdated</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int PROBLEM_LASTUPDATED_FEATURE_ID = 3;

    /**
     * The {@link ModelFactory} for this package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private ProblemandsolutionModelFactory factory;

    /**
     * The {@com.thedigitalstack.model.metadata.IDaoRegistry} for this package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final IDaoRegistry daoRegistry = new DaoRegistry(NS_URI); // NOSONAR

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    static {

        daoRegistry.registerDao(Solution.class, SolutionDao.class);

        daoRegistry.registerDao(Problem.class, ProblemDao.class);

    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public ProblemandsolutionModelPackage() {// NOSONAR
    }

    /**
     * Returns the {@link ProblemandsolutionModelFactory} of this ModelPackage.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the {@link ProblemandsolutionModelFactory} instance.
     * @generated
     */
    public ProblemandsolutionModelFactory getFactory() {
        return this.factory;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IDaoRegistry getDaoRegistry() {
        return daoRegistry;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    @NonNull
    protected Class<TenantModelPackage>[] getLinkedModelPackageClasses() {
        List<Class<? extends TenantModelPackage>> list = new ArrayList<Class<? extends TenantModelPackage>>();

        Class<TenantModelPackage>[] result = new Class[list.size()];
        list.toArray(result);
        return result;
    }

    /**
     * Initializes this {@link ModelPackage}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void doRegister(ResourceSet resourceSet, ModelResolver resolver) { // NOSONAR

        if (!registerModelPackageFromFile(resolver, resourceSet)) {
            return;
        }
        super.doRegister(resourceSet, resolver);
        factory = new ProblemandsolutionModelFactory(this);

        // read the model from the ecore file, the EPackage is registered in the EPackage.Registry
        // see the ModelResolver getEPackageRegistry method
        resolver.registerModelPackage(this);

        // register the relation between a Class and its EClassifier

        resolver.registerClassModelMapping(Solution.class, this.getSolutionEClass(), this);

        resolver.registerClassModelMapping(Problem.class, this.getProblemEClass(), this);

    }

    /**
     * Returns the {@link ModelFactory} of this ModelPackage.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the {@link ProblemandsolutionModelFactory} instance.
     * @generated
     */
    @Override
    public ProblemandsolutionModelFactory getModelFactory() {
        return factory;
    }

    /**
     * Returns the nsUri of the {@link EPackage} managed by this Package instance.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the nsUri of the EPackage
     * @generated
     */
    @Override
    public String getNsURI() {
        return NS_URI;
    }

    /**
     * Returns the name of the ecore file containing the ecore model of the {@link EPackage} managed here.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the name of the ecore file
     * @generated
     */
    @Override
    public String getEcoreFileName() {
        return "problemandsolution.ecore";
    }

    /**
     * Returns the {@link EClass} '<em><b>Solution</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EClass} '<em><b>Solution</b></em>'
     * @generated
     */
    public EClass getSolutionEClass() {
        return (EClass) getEPackage().getEClassifiers().get(SOLUTION_CLASSIFIER_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.id</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.id</b></em>'.
     * @generated
     */

    public EAttribute getSolution_Id() {
        return (EAttribute) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_ID_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.code</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.code</b></em>'.
     * @generated
     */

    public EAttribute getSolution_Code() {
        return (EAttribute) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_CODE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.description</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.description</b></em>'.
     * @generated
     */

    public EAttribute getSolution_Description() {
        return (EAttribute) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_DESCRIPTION_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.deliveryDate</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.deliveryDate</b></em>'.
     * @generated
     */

    public EAttribute getSolution_DeliveryDate() {
        return (EAttribute) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_DELIVERYDATE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.releasedIn</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.releasedIn</b></em>'.
     * @generated
     */

    public EAttribute getSolution_ReleasedIn() {
        return (EAttribute) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_RELEASEDIN_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Solution.problemRel</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Solution.problemRel</b></em>'.
     * @generated
     */

    public EReference getSolution_ProblemRel() {
        return (EReference) getSolutionEClass().getEAllStructuralFeatures().get(SOLUTION_PROBLEMREL_FEATURE_ID);
    }

    /**
     * Returns the {@link EClass} '<em><b>Problem</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EClass} '<em><b>Problem</b></em>'
     * @generated
     */
    public EClass getProblemEClass() {
        return (EClass) getEPackage().getEClassifiers().get(PROBLEM_CLASSIFIER_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Problem.id</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Problem.id</b></em>'.
     * @generated
     */

    public EAttribute getProblem_Id() {
        return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(PROBLEM_ID_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Problem.code</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Problem.code</b></em>'.
     * @generated
     */

    public EAttribute getProblem_Code() {
        return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(PROBLEM_CODE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Problem.description</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Problem.description</b></em>'.
     * @generated
     */

    public EAttribute getProblem_Description() {
        return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(PROBLEM_DESCRIPTION_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>Problem.lastUpdated</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>Problem.lastUpdated</b></em>'.
     * @generated
     */

    public EAttribute getProblem_LastUpdated() {
        return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(PROBLEM_LASTUPDATED_FEATURE_ID);
    }

    /**
     * Returns the class implementing a specific {@link EClass}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param eClassifier
     *            the {@link EClassifier}
     * @return the class implementing a specific {@link EClass}.
     * @generated
     */
    @Override
    public Class<?> getEClassifierClass(EClassifier eClassifier) {
        switch (eClassifier.getClassifierID()) {

            case SOLUTION_CLASSIFIER_ID:
                return Solution.class;

            case PROBLEM_CLASSIFIER_ID:
                return Problem.class;

        }
        throw new IllegalArgumentException("The EClassifier '" + eClassifier + "' is not defined in this EPackage");
    }

    /**
     * The adapter/wrapper for the EClass '<em><b>Solution</b></em>'.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */

    @XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "solutions")
    public static class SolutionXMLContainer implements IContainerList<Solution> {

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        private List<Solution> items;

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public SolutionXMLContainer() { // NOSONAR
            this.items = new ArrayList<Solution>(0);
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public SolutionXMLContainer(List<Solution> items) { // NOSONAR
            this.items = items;
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public SolutionXMLContainer(Solution item) { // NOSONAR
            this.items = Collections.singletonList(item);
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        @XmlElement(name = "solution")
        public List<Solution> getList() { // NOSONAR
            return items;
        }

    }

    /**
     * The adapter/wrapper for the EClass '<em><b>Problem</b></em>'.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */

    @XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "problems")
    public static class ProblemXMLContainer implements IContainerList<Problem> {

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        private List<Problem> items;

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public ProblemXMLContainer() { // NOSONAR
            this.items = new ArrayList<Problem>(0);
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public ProblemXMLContainer(List<Problem> items) { // NOSONAR
            this.items = items;
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        public ProblemXMLContainer(Problem item) { // NOSONAR
            this.items = Collections.singletonList(item);
        }

        /**
         * <!-- begin-user-doc --><!-- end-user-doc -->
         * 
         * @generated
         */
        @XmlElement(name = "problem")
        public List<Problem> getList() { // NOSONAR
            return items;
        }

    }

    /**
     * Returns the class container of the list. <br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the class container of the list {@link EClass}.
     * @generated
     */
    public Collection<Class<?>> getContainers() {
        Collection<Class<?>> containers = new ArrayList<Class<?>>();

        containers.add(SolutionXMLContainer.class);

        containers.add(ProblemXMLContainer.class);

        return containers;
    }

    /**
     * Creates a container for a specific {@link EClass}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param eClass
     *            creates a Object instance for this EClass
     * @return an object that representing the container of the EClass instances
     * @generated
     */
    public Class<?> getContainer(EClass eClass) {
        switch (eClass.getClassifierID()) {

            case ProblemandsolutionModelPackage.SOLUTION_CLASSIFIER_ID:
                return SolutionXMLContainer.class;

            case ProblemandsolutionModelPackage.PROBLEM_CLASSIFIER_ID:
                return ProblemXMLContainer.class;
        }
        throw new IllegalArgumentException("The EClass '" + eClass.getName() + "' is not a valid EClass for this EPackage");
    }

    /**
     * Returns the instance of the model Package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the instance of the model Package
     * @generated
     */
    public static ProblemandsolutionModelPackage getInstance() {
        ProblemandsolutionModelPackage p = (ProblemandsolutionModelPackage) Models.getMetadataManager().getModelPackage(NS_URI);
        assert p != null;
        return p;
    }

}

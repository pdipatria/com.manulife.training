package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.Solution;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '<em><b>Solution</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class SolutionSyncEntity extends AbstractJPASyncEntity<Solution> implements ISyncEntity<Solution> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<Solution> getDelegate() {
        return new SolutionService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<Solution> getEntityClass() {
        return Solution.class;
    }

}

package com.finantix.problemandsolution.core;

import javax.persistence.Basic;
import javax.persistence.Embeddable;

/**
 * A representation of the model object '<em><b>ServiceResponse</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Embeddable()
public class ServiceResponse extends com.thedigitalstack.model.Embeddable

{
    /**
     * @generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Basic()
    private boolean error = false;

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Basic()
    private String reason = null;

    /**
     * Returns the value of '<em><b>error</b></em>' feature.
     * 
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @return the value of '<em><b>error</b></em>' feature
     * @generated
     */
    public boolean isError() {
        return error;
    }

    /**
     * Sets the '{@link ServiceResponse#isError() <em>error</em>}' feature.
     * 
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param newError
     *            the new value of the '{@link ServiceResponse#isError() error}' feature.
     * @generated
     */
    public void setError(boolean newError) {
        error = newError;

    }

    /**
     * Returns the value of '<em><b>reason</b></em>' feature.
     * 
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @return the value of '<em><b>reason</b></em>' feature
     * @generated
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the '{@link ServiceResponse#getReason() <em>reason</em>}' feature.
     * 
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param newReason
     *            the new value of the '{@link ServiceResponse#getReason() reason}' feature.
     * @generated
     */
    public void setReason(String newReason) {
        reason = newReason;

    }

}

package com.finantix.problemandsolution.core.service.custom;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.GetSolutionDetailRequest;
import com.finantix.problemandsolution.GetSolutionDetailResponse;
import com.finantix.problemandsolution.Problemandsolution;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.service.ProblemService;
import com.finantix.problemandsolution.core.service.SolutionService;
import com.finantix.training.problemandsolution.ws.consumer.ProblemAndSolutionWSFactory;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.converter.UpdateConverter;
import com.thedigitalstack.model.converter.UpdateResult;
import com.thedigitalstack.model.metadata.TenantModelPackage;
import com.thedigitalstack.model.persistence.RunnableInTransactionAdapter;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.ExecutionContext;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.restful.annotation.RSTransactional;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;


@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/" + com.finantix.problemandsolution.core.Solution.CLASS_ID)
public class CustomSolutionService extends SolutionService {

    @GET
     @Path("{id}")
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })

    @Override
    @Nullable
    public Solution get( @PathParam("id") @NonNull Object id) {
        return super.get(id);
    }

    
     @ResourceMethodSignature(output=UpdateResult.class)
    @Override
     @PUT
     @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @ParamConverter(UpdateConverter.class)
    @NonNull
    public Solution update(@NonNull Solution entity) {
        return super.update(entity);
    }
     
     
     @POST
     @Path("unlink/{solutionId}/{problemId}")
     @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     public Solution unlink(@NonNull @PathParam("solutionId") Long solutionId, @NonNull @PathParam("problemId") Long problemId) {
         final Solution solution = super.get(solutionId);
         
         if (solution == null) {
             return null;
         }
         
         final ProblemService problemService = ServiceFactory.newService(ProblemService.class);
         final Problem problem = problemService.get(problemId); 
         
         if (problem != null && solution.getProblemRel().contains(problem)) {
             solution.getProblemRel().remove(problem);
             super.update(solution);
             return solution;
         }
         
         return null;
     }
     
     
     @POST
     @Path("link/{solutionId}/{problemId}")
     @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     public Solution link(@NonNull @PathParam("solutionId") Long solutionId, @NonNull @PathParam("problemId") Long problemId) {
         Solution solution = super.get(solutionId);
         
         if (solution == null) {
             return null;
         }
         
         final ProblemService problemService = ServiceFactory.newService(ProblemService.class);
         final Problem problem = problemService.get(problemId); 
         
         if (problem == null) {
             return null;
         }
         
         if (! solution.getProblemRel().contains(problem)) {
             solution.getProblemRel().add(problem);
             solution = super.update(solution);
             return solution;
         } else {
             return null;
         }
     }
     
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Solution getSolutionDetailPost(@NonNull String code) {
        return null;
    }
        
    @RSTransactional(context=RSTransactional.NONE)
     @GET
     @Path("getSolutionDetail/{code}")
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Solution getSolutionDetail(@PathParam("code") @NonNull String code) {
        SolutionService solutionService = ServiceFactory.newService(SolutionService.class);
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Solution.class);
        IPredicate pByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getSolution_Code().getName(), code);

        List<Solution> all = solutionService.getAll(QueryParamsBuilders.andExpressionBuilder(pByCode).buildQueryParams(), null);
        if (all == null || all.isEmpty()) {
            return null;
        }
        
        final Solution sol = all.iterator().next();
        if (sol != null) {
            Problemandsolution service = ProblemAndSolutionWSFactory.newService();
            GetSolutionDetailRequest req = new GetSolutionDetailRequest();
            req.setCode(code);
            GetSolutionDetailResponse solutionDetailFromWS = service.getSolutionDetail(req);
            mergeToSolution(sol, solutionDetailFromWS);

            ExecutionContext.runLocal(new RunnableInTransactionAdapter() {
                @Override
                public void run() {
                    SolutionService solutionService = ServiceFactory.newService(SolutionService.class);
                    solutionService.update(sol);
                }
            });
        }
        
        return sol;
    }
     
    /*
     * Converts a WS response in a solution
     */
    private Solution mergeToSolution(Solution sol, GetSolutionDetailResponse res) {
        sol.setDeliveryDate(res.getDeliveryDate().toGregorianCalendar().getTime());
        sol.setDescription(res.getDescription());
        sol.setReleasedIn(res.getReleasedIn());
        
        ProblemService problemService = ServiceFactory.newService(ProblemService.class);
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Problem.class);
        
        List<String> problemRel = res.getProblemRel();
        for (String problem : problemRel) {
            IPredicate pByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getProblem_Code().getName(), res.getProblemRel());
            List<Problem> byProblemCode = problemService.getAll(QueryParamsBuilders.andExpressionBuilder(pByCode).buildQueryParams(), null);
            sol.setProblemRel(byProblemCode);
        }
        return sol;
    }
}

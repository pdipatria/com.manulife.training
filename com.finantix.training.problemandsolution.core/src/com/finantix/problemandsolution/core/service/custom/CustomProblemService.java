package com.finantix.problemandsolution.core.service.custom;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.service.ProblemService;
import com.finantix.problemandsolution.core.service.SolutionService;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.converter.IdConverter;
import com.thedigitalstack.model.converter.IdResult;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.metadata.TenantModelPackage;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.service.Count;
import com.thedigitalstack.model.service.IOutMetadata;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.util.Assert;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseHeader;
import com.webcohesion.enunciate.metadata.rs.ResponseHeaders;

@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/" + com.finantix.problemandsolution.core.Problem.CLASS_ID)
public class CustomProblemService extends ProblemService {

    @ResourceMethodSignature(output = IdResult.class)
    @Override
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @ParamConverter(IdConverter.class)
    @NonNull
    public Problem create(@NonNull Problem entity) {

        Problem aProblem = super.create(entity);

        final SolutionService newService = ServiceFactory.newService(SolutionService.class);

        Solution aSolution = getSolution(aProblem);
        newService.create(aSolution);
        return aProblem;
    }

    private @NonNull
    Solution getSolution(@NonNull Problem aProblem) {
        Solution aSolution = new Solution();
        aSolution.setCode("S" + aProblem.getCode());
        if (aProblem.getDescription() != null) {
            aSolution.setDescription("Solution for " + aProblem.getDescription());
        }

        aSolution.getProblemRel().add(aProblem);

        return aSolution;
    }

    @ResourceMethodSignature(queryParams = { @QueryParam("f"), @QueryParam("l"), @QueryParam("o"), @QueryParam("by"), @QueryParam("filter") }, output = Count.class)
    @Override
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("count")
    @ParamConverter(com.thedigitalstack.model.converter.CountConverter.class)
    public int countAll(@Context @NonNull IQueryParams queryParams) {
        return super.countAll();
    }
    
    
    // <!-- JAX-RS Annotation -->
    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Override
    @Nullable
    public Problem get(@PathParam("id") @NonNull Object id) {
        return super.get(id);
    }

    /**
     * Gets all {@link Problem} satisfying the given query restriction. If no query parameters are specified, all entities will be returned.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param queryParams
     *            the QueryParams map with the parameters to use in the query
     * @param outMetadata
     *            the IOutMetadata with the response metadata
     * @RSParam l (optional) Max amount of response elements (limit). Example <i>?l=10</i>
     * @RSParam o (optional) Offset in the data storage of the first response element. Example <i>?o=10</i>
     * @RSParam by (optional) Ordering criteria of response elements. Example <i>?by=aFieldName|desc</i>
     * @RSParam filter (optional) Filter criteria of the response. Example <i>?aFieldName=1|2|3</i>
     * 
     * @return a list of {@link Problem}
     */
     @ResponseHeaders (
     {
     @ResponseHeader( name = "meta-totalCount", description = "Number of existing elements with the query restriction."),
     @ResponseHeader( name = "meta-partialCount", description = "Number of elements in the response."),
     @ResponseHeader( name = "meta-offset", description = "Offset in the data storage of the first element of the response."),
     @ResponseHeader( name = "meta-next", description = "Offset of the next existing element in data storage not included in the response.")
     }
     )
     @ResourceMethodSignature(queryParams={@QueryParam("l"),@QueryParam("o"),@QueryParam("by"),@QueryParam("filter")},
     output=ProblemandsolutionModelPackage.ProblemXMLContainer.class)
    @Override
     @GET
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Problem> getAll(@Context @NonNull IQueryParams queryParams, @Context @Nullable IOutMetadata outMetadata) {
        return super.getAll(queryParams, outMetadata);
    }

    
     /**
      * Search for Solutions related to a given Problem.
      * 
      * @param problemCode code of Problem whose Solutions are searched for.
      * @return list of Solutions (possibly empty) related to a given problem (code)
      */
    @GET
    @Path("searchByProblemCode/{code}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Solution> searchByProblemCode(@NonNull @PathParam("code") String problemCode) {
        
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Solution.class);
        
        final EntityManager entityManager = Models.getPersistenceManager().getEntityManager(ProblemandsolutionModelPackage.NS_URI);
        Assert.isNotNull(entityManager);
        
        @SuppressWarnings("null")
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Solution> query = criteriaBuilder.createQuery(Solution.class);
        
        final Root<Solution> fromSolution = query.from(Solution.class);
        
        final Metamodel metamodel = entityManager.getMetamodel();
        EntityType<Solution> solutionMetaModel = metamodel.entity(Solution.class);
        final ListJoin<Solution, Problem> join = fromSolution.join(solutionMetaModel.getList(mp.getSolution_ProblemRel().getName(), Problem.class));
        
        final javax.persistence.criteria.Path<String> pProblemCode = join.get(mp.getProblem_Code().getName()); 
        query.where(criteriaBuilder.equal(pProblemCode, problemCode));
        
        final TypedQuery<Solution> createQuery = entityManager.createQuery(query);
        List<Solution> solutions = createQuery.getResultList();
        
        return solutions;
    }
    

    /**
     * Search for Solutions related to a given Problem.
     * 
     * @param problemId id of Problem whose Solutions are searched for.
     * @return list of Solutions (possibly empty) related to a given problem (id)
     */
    @GET
    @Path("searchByProductId/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Solution> searchByProblemId(@NonNull @PathParam("id") String problemId) {
        final ArrayList<Solution> solutions = new ArrayList<Solution>();
        
        final Problem problem = ServiceFactory.newService(ProblemService.class).get(problemId);
        final SolutionService solutionService = ServiceFactory.newService(SolutionService.class);
        
        final List<Solution> allSolutions = solutionService.getAll();
        
        for (Solution solution : allSolutions) {
            if (solution.getProblemRel().contains(problem)) {
                solutions.add(solution);
            }
        }
        
        return solutions;
    }
}
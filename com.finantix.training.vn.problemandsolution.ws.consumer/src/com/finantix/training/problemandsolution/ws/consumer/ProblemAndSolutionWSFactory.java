package com.finantix.training.problemandsolution.ws.consumer;

import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.finantix.problemandsolution.Problemandsolution;
import com.finantix.problemandsolution.ProblemandsolutionWS;

public class ProblemAndSolutionWSFactory {

    public static final URL wsdlLocation = ProblemAndSolutionWSFactory.class.getResource("/wsdl/consumer/problemandsolution/problemandsolution.wsdl");
    public final static QName SERVICE = new QName("http://www.finantix.com/problemandsolution/", "problemandsolutionWS");
    public static final String endpointURL = "http://127.0.0.1:18080/wsdl_tr_web/services/problemandsolutionPort";
    
    public static Problemandsolution newService() {
//        URL wsdlLocation = ProblemAndSolutionWSFactory.class.getResource("/consumer/problemandsolution/problemandsolution.wsdl");
//        QName SERVICE = new QName("http://www.finantix.com/problemandsolution/", "problemandsolutionWS");
//        String endpointURL = "http://127.0.0.1:18080/wsdl_tr_web/services/problemandsolutionPort";
        
        Problemandsolution service = new ProblemandsolutionWS(wsdlLocation, SERVICE).getProblemandsolutionPort();
        
        BindingProvider provider = (BindingProvider) service;
        Map<String, Object> context = provider.getRequestContext();

        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
        
        return service;
    }
}
